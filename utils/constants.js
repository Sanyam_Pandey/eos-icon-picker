module.exports = {
  FILLED_SVG: 'svg/',
  OUTLINED_SVG: 'svg-outlined/',
  OUTLINED_TEMP: 'svg-outlined-temp/',
  FILLED_TEMP: 'svg-temp/',
}
