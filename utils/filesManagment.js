const fs = require('fs');

const moveMissingSvgVersion = async ({
  outlineSvgDir,
  normalSvgDir,
  tempFolder
}) => {

  const exits = fs.existsSync(tempFolder)

  if (!exits) {
    fs.mkdirSync(tempFolder)
  }

  // Get all the outline icons version
  const outlineList = fs
    .readdirSync(outlineSvgDir, (err, file) => {
      if (err) console.log(err)
      return file
    })
    .filter((ele) => ele.includes('.svg'))

  const eosIcons = fs
    .readdirSync(normalSvgDir, (err, file) => {
      if (err) console.log(err)
      return file
    })
    .filter((ele) => ele.includes('.svg'))



  const filtered = eosIcons.filter(function (x) {
    return outlineList.indexOf(x) < 0
  })
  console.log('filtered: ', filtered);

  // Move the missing files to complete the outline version
  filtered.map((icon) => {
    fs.copyFile(`${normalSvgDir}/${icon}`, `${tempFolder}/${icon}`, (err) => {
      if (err) throw err
    })
  })

  return `Done moving ${filtered.length} files to complete the missing version`
}

module.exports = {
  moveMissingSvgVersion
}
