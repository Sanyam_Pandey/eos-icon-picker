const fs = require('fs');
const CustomizeSvg = require('./customizeSvg');
const zipFolder = require('zip-folder');
const svg2img = require('svg2img');
const { resolve } = require('path');

const tempDir = `${__dirname}/../temp/`;
class CustomizedIconsPack {
  constructor(payload, timestamp, svgDir = 'svg/') {
    this.payload = payload;
    this.icons = payload.icons;
    this.customizationConfig = payload.customizationConfig;
    this.exportAs = payload.exportAs;
    this.exportSize = payload.exportSize;
    this.timestamp = timestamp;
    this.svgDir = svgDir;
    this.iconsOutputPath = `${tempDir}dist_${this.timestamp}/${this.exportAs}/`;
  }

  createDirectory() {
    return new Promise((resolve, reject) => {
      if (!fs.existsSync(tempDir)) {
        fs.mkdirSync(tempDir);
      }
      fs.mkdirSync(`${tempDir}dist_${this.timestamp}/`);
      fs.mkdirSync(this.iconsOutputPath);
      resolve();
    });
  }

  // write config file to export folder for future usage
  addConfigFile() {
    fs.writeFileSync(
      `${tempDir}dist_${this.timestamp}/customizationConfig.json`,
      JSON.stringify(this.payload),
      (err) => {
        if (err) {
          console.log(err);
        }
      }
    );
  }

  zipFiles() {
    //making zip file of export folder
    zipFolder(`${tempDir}dist_${this.timestamp}`, `${tempDir}dist_${this.timestamp}.zip`, function (err) {
      if (err) {
        console.log('⛔️Some error occurred while generating zip file:', err);
      }
    });
  }

  svgToPng(iconName, pngSize) {
    return new Promise((resolve, reject) => {
      svg2img(`${this.iconsOutputPath}${iconName}.svg`, { width: pngSize, height: pngSize }, (error, buffer) => {
        if (error) {
          console.log(error);
          return reject();
        }
        fs.writeFileSync(`${this.iconsOutputPath}${iconName}.png`, buffer);
        resolve();
      });
    });
  }
  createSvg() {
    return new Promise((resolve, reject) => {
      for (let i = 0; i < this.icons.length; i++) {
        let customIconObject = new CustomizeSvg(
          `${this.svgDir}${this.icons[i]}.svg`,
          `${this.iconsOutputPath}${this.icons[i]}.svg`,
          this.customizationConfig
        );
        customIconObject.finalizeIcon();
      }
      resolve();
    });
  }

  createPng() {
    return new Promise((resolve) => {
      for (let i = 0; i < this.icons.length; i++) {
        this.svgToPng(this.icons[i], this.exportSize)
          .then(() => {
            fs.unlink(`${this.iconsOutputPath}${this.icons[i]}.svg`, () => {
              if (i === this.icons.length - 1) resolve();
            });
          })
          .catch((err) => {
            console.log(`Some error occurred while creating PNG file.\nError: ${err}`);
          });
      }
    });
  }

  finalize() {
    this.addConfigFile();
    this.zipFiles();
    console.log(`Customized icons pack created!`);
  }

  generatePack() {
    this.createDirectory()
      .then(() => {
        if (this.exportAs === 'svg') {
          this.createSvg().then(() => {
            this.finalize();
          });
        } else {
          this.createSvg().then(() => {
            this.createPng().then(() => {
              this.finalize();
            });
          });
        }
      })
      .catch((err) => {
        console.log(`Some error occurred while creating Customized Icons.\nError: ${err}`);
      });
  }
}

module.exports = CustomizedIconsPack;
